# NOTE THIS IS NO LONGER BEING MAINTAINED IF THIS DOES NOT WORK ANYMORE PLEASE BE AWARE OF THIS #


# Open Media Project #

The open media project aims to allow everyone to add their own media to this system and share it between their media players.

### Features ###

* This system aims to be compleatly modulated so the person can make their own systems if they wish to use it with.
* A core API will be avaliable to use with it.

### API CALLS ###

Link 200 for commands to the core
* Note that if a ID is sent in the link message it will be sent back with its reply (could be used to identify commands if theres a few going on or UUIDS for menus!)

//this is to check if the core can connect to the OMP back end.
	CON|INF
//This is to get song information from the core and also the uuid and uuid's this goes off the	song id retreived by blah
	SONG|GET|SONGID
	SONG|INF|SONGID
//This is the search call for looking up stuff and appending it to a list
	LIST|USER|UUID|INDEX|AMMOUNT
	LIST|LETTER|LETTER|INDEX|AMMOUNT
	LIST|SEARCH|TERM|INDEX|AMMOUNT
	LIST|GENRE|TERM|INDEX|AMMOUNT
	LIST|ARTIST|TERM|INDEX|AMMOUNT
	LIST|ALL|INDEX|AMMOUNT

Link 201 for connection replies - ACT is replied if the core could not connect when trying to process a api request.
	TRUE|CON
	FALSE|CON
	TRUE|ACT
	FALSE|ACT

LINK 202 for song uuids
	TRUE|SONGID|ARTIST|TRACK|SONGDURATION|UUID's
	FALSE|TRACKID

LINK 203 for song information
	TRUE|SONGID|ARTIST|TRACK|GENRE|ALBUMUUID|AVATARNAME|AVATARUUID
	FALSE|TRACKID

LINK 204 for search results the artist track and index are appened like this untill the results are finished
	TRUE|INDEX|ARTIST|TRACK|SONGID 
	FALSE

RETUN CALL FOR EDT
	SONG|EDT|TRUE|SONGID|ARTIST|TRACK|GENRE|NOTECARD|ALBUMUUID|PRIVATE
	SONG|EDT|FALSE

### User Documentation ###

Hello and thanks for grabbing the Open Media Project [OPM] you are going to love this thing for sure!

### What is the Open Media Project ###

The Open Media Project (OMP for short) is a platform that allows you to share, organise and synchronise your secondlife music with all of your media objects in secondlife and any objects that others have integrated into it! The Open Media Project aims to make playing songs easier, finding the song you want and sharing them between people on sl.

### How does it work? ###

So how does all of this work? I have developed a Core script which accepts API calls for you to search, display and grab songs from. With this you can integrate the collection of songs from the OMP with what you already have, or use the OMP Network as a back end for your products to allow them to have all your latest up to date sounds! The work is all done for you and free to use!

### Adding my music ###

So how do you add your own music into the OMP! Its simple! just rez out the OMP Music Synchronizer and add your notecards into it and it will do the rest, it will ask if you wish to overide any existing notecards that already have been synced and if you wish to make them private or public (this can always be changed later using the OMP Music Manager).

The Notecards should have the following format, The name of them can be anything you want so make them something for you to recall if you need to and the contense inside should be the time of each track

Number of time between sound cuts
UUID
UUID
UUID

So for example a song with a 10 second cut between each part would be

10
UUID
UUID
UUID
UUID

etc

### Managing My Music ###

After you have used the OMP Music Synchronizer all you need to do now is use the OMP Music Manager to change the track information. This is very simple to use and should be pretty self explainitory :)

### Developement ###

OMP has a Core script which is what it uses to get the details for songs it wants to play as well of showing the avalible songs from the Network. You can use the following api with your own products to do really anything media related on sl.
(update your typer or walker with your own uploaded sounds with a locked in search, make your own media player to sell with every song thats avalible on the OMP or just have it so avatars can have their own music displayed on it1)

### API Calls ###

The way that the core is communicated with is via link messages in lsl. The calls are sent to integer 200 and the replies are sent back to different numbers.

An example would be this llMessageLinked(LINK_SET, 200, "LIST|SEARCH|hats|0|9", NULL_KEY); This will search for a song with the name containing "hats"

Here is an exmaple for a Dialog Menu 􀀁
And another simple example for a Media Player 􀀀

## Connection ##

This is to check if the OMP Core is able to connect to the network the following command

    CON|INF

This will return the following on 201

    TRUE|CON     Connection successful using the CON|INF call
    FALSE|CON     Connection Failed using the CON|INF
    FALSE|ACT     This is returned if you try using any command and there is a connection error


## Searching ##

Searching is done in the following format

LIST|<Search Terms>|INDEX|TOTAL RESULTS

The INDEX and TOTAL RESULTS at the end of this is for optimising the results if there is a large amount of them (this would be great for using for menus and also text based results etc).
For example 0|9 would give you the first 9 results of your search query, 1|9 would give you the second 9, this would be very usefull for llDialog menus!

The search terms are done in the following 

    ALL                     This will just give you every available song
    USER|<User UUID>        This will only display public songs from this users uuid
    LETTER|<Letter>            This will narrow results to the starting letter(s) in the track
    SEARCH|<Search Term>    Search for a track name
    GENRE|<Genre>            Search for a specific genre
    ARTIST|<Artist>            Search for an artist

Feel free to look at the Menu example script to see some examples!

The Results will return on 204

    TRUE|INDEX|ARTIST|TRACK|SONGINDEX    The ARTIST, TRACK and SONGINDEX are repeated if there are multiple results.
    FALSE                                If there are no results it will just say FALSE

## Information ##

You can request information on a specific song (track name, artist, genre, album art and uploader)

    SONG|INF|<song id>		This will retrieve information on the selected song
    SONG|INF|RDM			This will retrieve information on a random song, can be used for a shuffle function.

The Results will return on 203

    TRUE|SONGID|ARTIST|TRACK|GENRE|ALBUMUUID|AVATARNAME|AVATARUUID    This is pretty self-explanatory.
    FALSE|SONGID                                                    This will happen if the song does not exist or the song is not public.

## Playing ##

    TRUE|SONGID|ARTIST|TRACK|SONGDURATION|UUID's     This call can be quite large since songs can have a large amount of uuid's
    FALSE|SONGID                                    This will happen if the song does not exist or the song is not public

### Who do I talk to? ###

* [Kurtis Anatine](https://my.secondlife.com/kurtis.anatine)