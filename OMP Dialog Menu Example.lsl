/*
	OpenMediaMenuExample.lsl - Made By Kurtis Anatine

	Notes:
			- This is an example on how to make a basic menu with the OpenMedia System
*/
// Global Lists 
list lst_listen =	 [
					0,		//current listen channel
					0,		//current listen integer
					0,		//current index of media
					"",		//Search API TYPE
					"",		//Term for API
					NULL_KEY	//uuid for the texture - Tells if a song has been loaded.
					];
list lst_buttonindex;
list lst_buttons;
list lst_mediainf = [
					"",			//Track String
					"",			//artist string
					"",			//genre string
					"",			//Uploader Name
					NULL_KEY,	//uploader uuid
					NULL_KEY 	//texture uuid
					];
// Global Variables
// Subroutines
list order_buttons(list buttons)
{
	return llList2List(buttons, -3, -1) + llList2List(buttons, -6, -4) +
		llList2List(buttons, -9, -7) + llList2List(buttons, -12, -10);
}
// Debug
// Main Script
default
{
	attach(key id)
	{
		if(id) 
		{
			llRequestPermissions(id, PERMISSION_TAKE_CONTROLS); 
		}
	}
	changed(integer change)
	{
		if(change & CHANGED_OWNER)
		{
			llResetScript();
		}	
	}
	run_time_permissions(integer perm)
	{
		if(perm & PERMISSION_TAKE_CONTROLS) 
		{
			llTakeControls( 0 , FALSE, TRUE);
		}
	}
	state_entry()
	{
		//llSetMemoryLimit( llGetUsedMemory()+10000);	//Memory limit if needed!
		lst_listen = llListReplaceList(lst_listen, (list)(-1 - (integer)("0x" + llGetSubString( (string) llGetKey(), -7, -1) )), 0, 0);
	}
	on_rez(integer start_param)
	{
		lst_listen = llListReplaceList(lst_listen, (list)(-1 - (integer)("0x" + llGetSubString( (string) llGetKey(), -7, -1) )), 0, 0);
		llListenRemove(llList2Integer(lst_listen, 1));	
	}
	touch_start(integer num_detected)
	{
		if(llDetectedKey(0) == llGetOwner())
		{
			lst_listen = llListReplaceList(lst_listen, (list)0, 2, 2);
			llListenRemove(llList2Integer(lst_listen, 1));
			lst_listen = llListReplaceList(lst_listen, [llListen(llList2Integer(lst_listen, 0), "", llGetOwner(), "")], 1, 1);
			llSetTimerEvent(30);
			llDialog(llGetOwner(), "Open Media Project Player\n--------------------\nHi there!\nPlease Choose one of the following Options!\nMine: This will only display your own songs.\nFirst Letter: This will display songs beginning with the letter (or letters) you put in\nSearch: search for a part of the track name\nArtist: Search by track artist\nGenre: Search by genre", order_buttons(["All","Mine","First Letter","Search","Artist","Genre"," ","Random"," ","Play","Exit","Stop"]), llList2Integer(lst_listen, 0));
		}	
	}
	link_message(integer sender_num, integer num, string str, key id)
	{
		if (num == 204)
		{
			list command = llParseString2List(str,["|"],[]);
			str = "";
			if(llList2Integer(command, 0) == TRUE)
			{// build the menu and args
				command = llDeleteSubList(command, 0, 0);
				lst_listen = llListReplaceList(lst_listen, (list)llList2Integer(command, 0), 2, 2);
				command = llDeleteSubList(command, 0, 0);
				lst_buttonindex = [];
				lst_buttons = [];
				integer buttonbase = llList2Integer(lst_listen, 2) * 9;
				string menu = "OpenMedia Project Menu\n--------------------\n";
				while (llGetListLength(command) != 0)
				{
					lst_buttons += (string)(llGetListLength(lst_buttonindex) + buttonbase);
					menu += (string)(llGetListLength(lst_buttonindex) + buttonbase)+": ";
					if(llStringLength(llList2String(command, 1)) > 26)
					{
						menu += llGetSubString(llList2String(command, 1),0,25)+"~";
					}
					else
					{
						menu += llList2String(command, 1);
					}
					if(llStringLength(llList2String(command, 0)) > 14)
					{
						menu += " by "+llGetSubString(llList2String(command, 0),0,12)+"~"+"\n";
					}
					else
					{
						menu += " by "+llList2String(command, 0)+"\n";
					}
					lst_buttonindex += llList2Integer(command, 2);
					command = llDeleteSubList(command, 0, 2);
				}
				menu += "--------------------\n";
				lst_listen = llListReplaceList(lst_listen, [llListen(llList2Integer(lst_listen, 0), "", llGetOwner(), "")], 1, 1);
				llSetTimerEvent(30);
				llDialog(llGetOwner(), menu, order_buttons(lst_buttons+["Prev","Back","Next"]), llList2Integer(lst_listen, 0));
			}
			else
			{
				lst_listen = llListReplaceList(lst_listen, [llListen(llList2Integer(lst_listen, 0), "", llGetOwner(), "")], 1, 1);
				llSetTimerEvent(30);
				llDialog(llGetOwner(), "OpenMedia Project Menu\n--------------------\nThere where no results found! Try again :x", order_buttons([" ","Back"," "]), llList2Integer(lst_listen, 0));
			}
		}
		else if (num == 203)
		{
			list command = llParseString2List(str,["|"],[]);
			str = "";
			if(llList2Integer(command, 0) == TRUE)
			{
				llMessageLinked(LINK_SET, 200, "SONG|GET|"+llList2String(command, 1), NULL_KEY);
				lst_mediainf = llListReplaceList(lst_mediainf, (list)llList2String(command, 3), 0, 0);
				lst_mediainf = llListReplaceList(lst_mediainf, (list)llList2String(command, 2), 1, 1);
				lst_mediainf = llListReplaceList(lst_mediainf, (list)llList2String(command, 4), 2, 2);
				lst_mediainf = llListReplaceList(lst_mediainf, (list)llList2String(command, 6), 3, 3);
				lst_mediainf = llListReplaceList(lst_mediainf, (list)llList2Key(command, 7), 4, 4);
				lst_mediainf = llListReplaceList(lst_mediainf, (list)llList2Key(command, 5), 5, 5);
				llSetTexture(llList2Key(lst_mediainf, 5), 4);
				llSetTexture(llList2Key(lst_mediainf, 5), 2);
				string message =	 "OpenMedia Project Menu\n--------------------\n"+
							"Playing: "+llList2String(lst_mediainf, 0)+"\n"+
							"Artist: "+llList2String(lst_mediainf, 1)+"\n"+
							"Genre: "+llList2String(lst_mediainf, 2)+"\n"+
							"Uploaded By "+llList2String(lst_mediainf, 3)+"\n"+
							"--------------------\n"
				;
				lst_listen = llListReplaceList(lst_listen, [llListen(llList2Integer(lst_listen, 0), "", llGetOwner(), "")], 1, 1);
				llSetTimerEvent(30);
				llDialog(llGetOwner(), message,  order_buttons([" Genre ","Uploader"," Artist "," ","Back"," "]), llList2Integer(lst_listen, 0));
			}
			else
			{
				lst_listen = llListReplaceList(lst_listen, [llListen(llList2Integer(lst_listen, 0), "", llGetOwner(), "")], 1, 1);
				llSetTimerEvent(30);
				llDialog(llGetOwner(), "Open Media Project Menu\n--------------------\nThere was an error loading the track!\n Please Try again :x", order_buttons([" ","Back"," "]), llList2Integer(lst_listen, 0));
			}
		}
		else if (num == 5)
		{
			if(str == "EMPTY")
			{
				lst_listen = llListReplaceList(lst_listen, (list)"ALL", 3, 3);
				lst_listen = llListReplaceList(lst_listen, (list)"", 4, 4);
				lst_listen = llListReplaceList(lst_listen, (list)0, 2, 2);
				llMessageLinked(LINK_SET, 200, "LIST|ALL|0|9", NULL_KEY);
			}
		}
		else if (num == 201)
		{
			list command = llParseString2List(str,["|"],[]);
			str = "";
			if (llList2Integer(command, 0) == FALSE)
			{
				lst_listen = llListReplaceList(lst_listen, [llListen(llList2Integer(lst_listen, 0), "", llGetOwner(), "")], 1, 1);
				llSetTimerEvent(30);
				llDialog(llGetOwner(), "Open Media Project Player\n--------------------\nThere was a connection error :/ You might have to try again later X_X", order_buttons([" ","Back"," "]), llList2Integer(lst_listen, 0));
			}
		}
	}
	listen(integer channel, string name, key id, string message)
	{
		llSetTimerEvent(0);
		llListenRemove(llList2Integer(lst_listen, 1));
		if(message == " Genre ")
		{
			lst_listen = llListReplaceList(lst_listen, (list)"GENRE", 3, 3);
			lst_listen = llListReplaceList(lst_listen, (list)llList2String(lst_mediainf, 2), 4, 4);
			lst_listen = llListReplaceList(lst_listen, (list)0, 2, 2);
			llMessageLinked(LINK_SET, 200, "LIST|"+llList2String(lst_listen, 3)+"|"+llList2String(lst_listen, 4)+"|"+llList2String(lst_listen, 2)+"|9", NULL_KEY);
		}
		else if(message == "Play")
		{
			if (llList2String(lst_mediainf, 0) != "")
			{
				llMessageLinked(LINK_SET, 4, "PLAY", NULL_KEY);
				llSetTexture(llList2Key(lst_mediainf, 5), 4);
				llSetTexture(llList2Key(lst_mediainf, 5), 2);
				string message =	 
					"OpenMedia Project Menu\n--------------------\n"+
					"Playing: "+llList2String(lst_mediainf, 0)+"\n"+
					"Artist: "+llList2String(lst_mediainf, 1)+"\n"+
					"Genre: "+llList2String(lst_mediainf, 2)+"\n"+
					"Uploaded By "+llList2String(lst_mediainf, 3)+"\n"+
					"--------------------\n"
					;
				lst_listen = llListReplaceList(lst_listen, [llListen(llList2Integer(lst_listen, 0), "", llGetOwner(), "")], 1, 1);
				llSetTimerEvent(30);
				llDialog(llGetOwner(), message,  order_buttons([" Genre ","Uploader"," Artist "," ","Back"," "]), llList2Integer(lst_listen, 0));
			}
			else
			{
				lst_listen = llListReplaceList(lst_listen, (list)"ALL", 3, 3);
				lst_listen = llListReplaceList(lst_listen, (list)"", 4, 4);
				lst_listen = llListReplaceList(lst_listen, (list)0, 2, 2);
				llMessageLinked(LINK_SET, 200, "LIST|ALL|0|9", NULL_KEY);
			}
		}
		else if(message == "Stop")
		{
			llMessageLinked(LINK_SET, 4, "STOP", NULL_KEY);
			llSetTexture(TEXTURE_BLANK, 4);
			llSetTexture(TEXTURE_BLANK, 2);
			lst_listen = llListReplaceList(lst_listen, [llListen(llList2Integer(lst_listen, 0), "", llGetOwner(), "")], 1, 1);
			llSetTimerEvent(30);
			llDialog(llGetOwner(), "Open Media Project Player\n--------------------\nHi there!\nPlease Choose one fo the following Options!\nMine: This will only display your own songs.\nFirst Letter: This will display songs beginning with the letter (or letters) you put in\nSearch: search for a part of the track name\nArtist: Search by track artist\nGenre: Search by genre", order_buttons(["All","Mine","First Letter","Search","Artist","Genre"," ","Random"," ","Play","Exit","Stop"]), llList2Integer(lst_listen, 0));
		}
		else if(message == "Uploader")
		{
			lst_listen = llListReplaceList(lst_listen, (list)"USER", 3, 3);
			lst_listen = llListReplaceList(lst_listen, (list)llList2Key(lst_mediainf, 4), 4, 4);
			lst_listen = llListReplaceList(lst_listen, (list)0, 2, 2);
			llMessageLinked(LINK_SET, 200, "LIST|"+llList2String(lst_listen, 3)+"|"+llList2String(lst_listen, 4)+"|"+llList2String(lst_listen, 2)+"|9", NULL_KEY);
		}
		else if(message == " Artist ")
		{
			lst_listen = llListReplaceList(lst_listen, (list)"ARTIST", 3, 3);
			lst_listen = llListReplaceList(lst_listen, (list)llList2String(lst_mediainf, 1), 4, 4);
			lst_listen = llListReplaceList(lst_listen, (list)0, 2, 2);
			llMessageLinked(LINK_SET, 200, "LIST|"+llList2String(lst_listen, 3)+"|"+llList2String(lst_listen, 4)+"|"+llList2String(lst_listen, 2)+"|9", NULL_KEY);
		}
		else if(message == "All")
		{
			lst_listen = llListReplaceList(lst_listen, (list)"ALL", 3, 3);
			lst_listen = llListReplaceList(lst_listen, (list)"", 4, 4);
			lst_listen = llListReplaceList(lst_listen, (list)0, 2, 2);
			llMessageLinked(LINK_SET, 200, "LIST|ALL|0|9", NULL_KEY);
		}
		else if(message == "Mine")
		{
			lst_listen = llListReplaceList(lst_listen, (list)"USER", 3, 3);
			lst_listen = llListReplaceList(lst_listen, (list)llGetOwner(), 4, 4);
			lst_listen = llListReplaceList(lst_listen, (list)0, 2, 2);
			llMessageLinked(LINK_SET, 200, "LIST|"+llList2String(lst_listen, 3)+"|"+llList2String(lst_listen, 4)+"|0|9", NULL_KEY);
		}
		else if(message == "First Letter")
		{
			lst_listen = llListReplaceList(lst_listen, (list)"LETTER", 3, 3);
			lst_listen = llListReplaceList(lst_listen, (list)"", 4, 4);
			lst_listen = llListReplaceList(lst_listen, (list)0, 2, 2);
			lst_listen = llListReplaceList(lst_listen, [llListen(llList2Integer(lst_listen, 0), "", llGetOwner(), "")], 1, 1);
			llSetTimerEvent(30);
			llTextBox(llGetOwner(), "OpenMedia Project\n--------------------\nPlease enter your first letter(s) you want to filter with.", llList2Integer(lst_listen, 0));
		}
		else if(message == "Search")
		{
			lst_listen = llListReplaceList(lst_listen, (list)"SEARCH", 3, 3);
			lst_listen = llListReplaceList(lst_listen, (list)llGetOwner(), 4, 4);
			lst_listen = llListReplaceList(lst_listen, (list)0, 2, 2);
			lst_listen = llListReplaceList(lst_listen, [llListen(llList2Integer(lst_listen, 0), "", llGetOwner(), "")], 1, 1);
			llSetTimerEvent(30);
			llTextBox(llGetOwner(), "OpenMedia Project\n--------------------\nPlease enter the track you wish to search for.", llList2Integer(lst_listen, 0));
		}
		else if(message == "Artist")
		{
			lst_listen = llListReplaceList(lst_listen, (list)"ARTIST", 3, 3);
			lst_listen = llListReplaceList(lst_listen, (list)"", 4, 4);
			lst_listen = llListReplaceList(lst_listen, (list)0, 2, 2);
			lst_listen = llListReplaceList(lst_listen, [llListen(llList2Integer(lst_listen, 0), "", llGetOwner(), "")], 1, 1);
			llSetTimerEvent(30);
			llTextBox(llGetOwner(), "OpenMedia Project\n--------------------\nPlease type in the name of the artist you wish to search for.", llList2Integer(lst_listen, 0));
		}
		else if(message == "Genre")
		{
			lst_listen = llListReplaceList(lst_listen, (list)"GENRE", 3, 3);
			lst_listen = llListReplaceList(lst_listen, (list)"", 4, 4);
			lst_listen = llListReplaceList(lst_listen, (list)0, 2, 2);
			lst_listen = llListReplaceList(lst_listen, [llListen(llList2Integer(lst_listen, 0), "", llGetOwner(), "")], 1, 1);
			llSetTimerEvent(30);
			llTextBox(llGetOwner(), "OpenMedia Project\n--------------------\nPlease type in the genre you wish to search for.", llList2Integer(lst_listen, 0));
		}
		else if(message == "Prev")
		{
			if (llList2String(lst_listen, 3) == "ALL")
			{
				llMessageLinked(LINK_SET, 200, "LIST|ALL|"+(string)(llList2Integer(lst_listen, 2) - 1)+"|9", NULL_KEY);
			}
			else
			{
				llMessageLinked(LINK_SET, 200, "LIST|"+llList2String(lst_listen, 3)+"|"+llList2String(lst_listen, 4)+"|"+(string)(llList2Integer(lst_listen, 2) - 1)+"|9", NULL_KEY);
			}
		}
		else if(message == "Next")
		{
			if (llList2String(lst_listen, 3) == "ALL")
			{
				llMessageLinked(LINK_SET, 200, "LIST|ALL|"+(string)(llList2Integer(lst_listen, 2) + 1)+"|9", NULL_KEY);
			}
			else
			{
				llMessageLinked(LINK_SET, 200, "LIST|"+llList2String(lst_listen, 3)+"|"+llList2String(lst_listen, 4)+"|"+(string)(llList2Integer(lst_listen, 2) + 1)+"|9", NULL_KEY);
			}
		}
		else if(message == "Exit" || message == " " || message == "")
		{

		}
		else if(message == "Random")
		{
			llMessageLinked(LINK_SET, 200, "SONG|INF|RDM", id);
		}
		else if(message == "Back")
		{
			lst_listen = llListReplaceList(lst_listen, (list)0, 2, 2);
			llListenRemove(llList2Integer(lst_listen, 1));
			lst_listen = llListReplaceList(lst_listen, [llListen(llList2Integer(lst_listen, 0), "", llGetOwner(), "")], 1, 1);
			llSetTimerEvent(30);
			llDialog(llGetOwner(), "Open Media Project Player\n--------------------\nHi there!\nPlease Choose one fo the following Options!\nMine: This will only display your own songs.\nFirst Letter: This will display songs beginning with the letter (or letters) you put in\nSearch: search for a part of the track name\nArtist: Search by track artist\nGenre: Search by genre", order_buttons(["All","Mine","First Letter","Search","Artist","Genre"," ","Random"," ","Play","Exit","Stop"]), llList2Integer(lst_listen, 0));
		}
		else if(llListFindList(lst_buttons, (list)message) != -1)
		{
			llMessageLinked(LINK_SET, 200, "SONG|INF|"+llList2String(lst_buttonindex, llListFindList(lst_buttons, (list)message)), NULL_KEY);
		}
		else
		{
			lst_listen = llListReplaceList(lst_listen, (list)message, 4, 4);
			llMessageLinked(LINK_SET, 200, "LIST|"+llList2String(lst_listen, 3)+"|"+llList2String(lst_listen, 4)+"|"+llList2String(lst_listen, 2)+"|9", NULL_KEY);
		}
	}
	timer()
	{
		llSetTimerEvent(0);
		llListenRemove(llList2Integer(lst_listen, 1));
		llOwnerSay("Menu has timed out");
	}
}
