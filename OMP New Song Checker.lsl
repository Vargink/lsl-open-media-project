/*
	[TC] Template.lsl - Made By Kurtis Anatine

	Notes:
			- Can be put into any prim. This is just a template
*/
// Global Lists
list lst_music	=	[
					0			// 0 Song INDEX
					];
// Global Variables
// Functions
// Main Script
default
{
	state_entry()
	{
		llSetMemoryLimit( llGetUsedMemory()+2000 );
		llSetTimerEvent(60);
		llMessageLinked(LINK_SET, 200, "LIST|ALL|-1|1", NULL_KEY);
	}
	link_message(integer sender_num, integer num, string str, key id)
	{
		if(num == 204)
		{
			list command = llParseString2List(str, ["|"], []);
			if(llList2Integer(command, 0))
			{
				if(llList2Integer(command, 1) > llList2Integer(lst_music, 0))
				{
					lst_music = llListReplaceList(lst_music, [llList2Integer(command, 1)], 0, 0);
					llSay(0, "A new song has been uploaded! The total ammount of songs: "+llList2String(lst_music, 0));
				}
			}
			else
			{
				llSay(0,"There was an error trying to update. Please Try again later!");
			}
		}	
	}
	timer()
	{
		llMessageLinked(LINK_SET, 200, "LIST|ALL|-1|1", NULL_KEY);
	}
}
