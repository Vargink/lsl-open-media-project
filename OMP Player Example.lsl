/*
	OpenMediaPlayerExample.lsl - Made By Kurtis Anatine

	Notes:
			- This is an example script that will work with the 
*/
// Global Lists
list uuids;
integer index;
float timeint;
// Global Variables
// Subroutines
// Debug
// Main Script
default
{
	attach(key id)
	{
		if(id) 
		{
			llRequestPermissions(id, PERMISSION_TAKE_CONTROLS); 
		}
	}
	run_time_permissions(integer perm)
	{
		if(perm & PERMISSION_TAKE_CONTROLS) 
		{
			llTakeControls( 0 , FALSE, TRUE);
		}
	}
    state_entry()
    {
        //llSetMemoryLimit( llGetUsedMemory()+10000);	//Memory limit if needed!
    }
    link_message(integer sender_num, integer num, string str, key id)
    {
    	if (num == 202)
    	{

    		list command = llParseString2List(str,["|"],[]);
			str = "";
			if(llList2Integer(command, 0) == TRUE)
			{
				uuids = [];
    			index = 0;
				llSay(0, "Loading "+llList2String(command, 2)+" by "+llList2String(command, 1));
				command = llDeleteSubList(command, 0, 2);
				timeint = llList2Float(command, 0);
				command = llDeleteSubList(command, 0, 0);
				while (llGetListLength(command) != 0)
				{
					uuids += llList2Key(command, 0);
					command = llDeleteSubList(command, 0, 0);
				}
				llPreloadSound(llList2Key(uuids, 0));
				if(2 < llGetListLength(uuids)){llPreloadSound(llList2Key(uuids, 1));}
				if(3 < llGetListLength(uuids)){llPreloadSound(llList2Key(uuids, 3));}
				llPlaySound(llList2Key(uuids, 0), 1);
				llSetTimerEvent(timeint);
			}
			else
			{
				llSay(0, "The Song Failed to load, Songid: "+llList2String(command, 1));
			}
    	}	
    }
    timer()
    {
    	++index;
    	if(index == llGetListLength(uuids))
    	{
    		llSetTimerEvent(0);
    	}
    	else
    	{
    		llPlaySound(llList2Key(uuids, index), 1);
    		if(index + 1 < llGetListLength(uuids)){llPreloadSound(llList2Key(uuids, index + 1));}
    		if(index + 2 < llGetListLength(uuids)){llPreloadSound(llList2Key(uuids, index + 2));}
    		if(index + 3 < llGetListLength(uuids)){llPreloadSound(llList2Key(uuids, index + 3));}
    	}
    }
}
